#!/usr/bin/env python3

from dataclasses import dataclass
import shlex
import sys

REMOTE_IP = sys.argv[1]
ARGS='-T 3600'

@dataclass
class Experiment:
	env: str		# 'local' or 'remote'
	lang: str		# 'python' or 'java'
	driver: str		# 'old' or 'new'
	server: str		# 'oct2020' or 'default'

	@property
	def filename(self):
		return f"{self.env}-{self.lang}-{self.driver}-{self.server}.csv"

	@property
	def tag(self):
		return f"{self.env},{self.lang},{self.driver},{self.server}"

	@property
	def port(self):
		return dict(oct2020=55000, default=55001)[self.server]

	@property
	def host(self):
		return dict(local='localhost', remote='$(REMOTE_IP)')[self.env]

	@property
	def command(self):
		if self.lang == 'java':
			jar = dict(
				old='java-conn-bench-2-29.jar',
				new='java-conn-bench-3.0.jar'
			)[self.driver]
			cmd = ['java', '-jar', jar]
		elif self.lang == 'python':
			cmd = ['python3', 'benchconn.py', '-c', self.driver]
		else:
			assert False

		cmd += ['-p', str(self.port)]
		cmd += ['-d', f'db{self.port}']
		cmd += ['-H', self.host]

		return cmd


EXPERIMENTS = [
	Experiment(env=env, lang=lang, driver=driver, server=server)
	for env in ['local', 'remote']
	for lang in ['python', 'java']
	for driver in ['old', 'new']
	for server in ['oct2020', 'default']
]

print('REMOTE_IP = ' + REMOTE_IP)
print()

print(f"experiment.csv: {' '.join(e.filename for e in EXPERIMENTS)}")
print(f"	cat {' '.join(e.filename for e in EXPERIMENTS)} >$@")
print()

qme = shlex.quote(sys.argv[0])
print("Makefile: " + qme)
print(f"\t{qme} {REMOTE_IP}>$@.tmp")
print("\tmv $@.tmp $@")

for e in EXPERIMENTS:
	cmd = e.command
	cmd += shlex.split(ARGS)
	cmd += [ '-l', e.tag]
	qcmd = " ".join(shlex.quote(a) for a in cmd)
	print(f"{e.filename}:")
	print(f"\tnc {e.host} {e.port + 1000} </dev/null")
	print(f"\t{qcmd} > $@.tmp")
	print("\tmv $@.tmp $@")
	print()

