package com.monetdbsolutions;

import net.sourceforge.argparse4j.ArgumentParsers;
import net.sourceforge.argparse4j.inf.ArgumentParser;
import net.sourceforge.argparse4j.inf.ArgumentParserException;
import net.sourceforge.argparse4j.inf.Namespace;

import java.sql.*;
import java.text.MessageFormat;
import java.util.concurrent.ArrayBlockingQueue;

/**
 * Hello world!
 */
public class App {
	public static void main(String[] args) throws SQLException, AppException, InterruptedException {
		try {
			Class.forName("org.monetdb.jdbc.MonetDriver");
		} catch (ClassNotFoundException e) {
			// ignore
		}
		try {
			Class.forName("nl.cwi.monetdb.jdbc.MonetDriver");
		} catch (ClassNotFoundException e) {
			// ignore
		}

		ArgumentParser argparser = ArgumentParsers.newFor("java-conn-bench").build();
		argparser.addArgument("-H", "--host")
				.type(String.class)
				.setDefault("localhost")
		;
		argparser.addArgument("-p", "--port")
				.type(Integer.class)
				.setDefault(50000)
		;
		argparser.addArgument("-d", "--database")
				.type(String.class)
				.required(true)
		;
		argparser.addArgument("-m", "--min-connections")
				.type(Integer.class)
				.setDefault(1000)
		;
		argparser.addArgument("-M", "--max-connections")
				.type(Integer.class)
		;
		argparser.addArgument("-t", "--min-time")
				.type(Integer.class)
				.setDefault(5)
		;
		argparser.addArgument("-T", "--max-time")
				.type(Long.class)
		;
		argparser.addArgument("-l", "--label")
				.type(String.class)
				.required(true)
		;
		Namespace ns = null;
		try {
			ns = argparser.parseArgs(args);
		} catch (ArgumentParserException e) {
			System.err.println(e.getMessage());
			System.exit(1);
		}
		if (ns.getString("max_connections") == null && ns.getLong("max_time") == null) {
			System.err.println("Either -M/--max-connections or -T/--max-time should be set");
			System.exit(1);
		}

		experiment(ns);
	}

	private static void experiment(Namespace args) throws SQLException, AppException, InterruptedException {
		Closer closer = new Closer();

		int min_conns = args.getInt("min_connections");
		Integer max_conns = args.getInt("max_connections");
		if (max_conns != null && max_conns < min_conns) {
			min_conns = max_conns;
		}
		long min_time = args.getInt("min_time") * 1000;
		Long max_time = args.getLong("max_time");
		if (max_time != null) {
			max_time *= 1000;
			if (max_time < min_time) {
				min_time = max_time;
			}
		}
		attempt(closer, args);

		long t0 = System.currentTimeMillis();
		int connections = 0;
		while (true) {
			long duration = System.currentTimeMillis() - t0;
			if (duration >= min_time && connections >= min_conns) {
				// consider stopping
				if (max_time != null && duration >= max_time) {
					break;
				}
				if (max_conns != null && connections >= max_conns) {
					break;
				}
			}
			connections += attempt(closer, args);
		}

		double duration = (System.currentTimeMillis() - t0) / 1000.0;
		double rate = connections / duration;
		System.out.printf("%s,%d,%s,%s%n", args.getString("label"), connections, duration, rate);


	}

	private static int attempt(Closer closer, Namespace args) throws SQLException, AppException, InterruptedException {
		String url = String.format("jdbc:monetdb://%s:%d/%s",
				args.getString("host"),
				args.getInt("port"),
				args.getString("database"));
		for (int i = 0; i < 100; i++) {
			Connection conn = DriverManager.getConnection(url, "monetdb", "monetdb");
			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery("select 42");
			if (!rs.next()) {
				throw new AppException("Query return no rows");
			}
			int n = rs.getInt(1);
			if (n != 42) {
				throw new AppException(MessageFormat.format("Query returned {0}, not 42", n));
			}
			closer.close(conn, rs);
		}

		return 100;
	}

	static class AppException extends Exception {
		public AppException(String msg) {
			super(msg);
		}
	}

	static class Closer implements Runnable {
		final ArrayBlockingQueue<Item> queue;
		final Thread worker;

		Closer() {
			queue = new ArrayBlockingQueue<>(60);
			worker = new Thread(this);
			worker.setDaemon(true);
			worker.start();
		}

		void close(Connection conn, ResultSet rs) throws InterruptedException {
			Item item = new Item(conn, rs);
			queue.put(item);
		}

		@Override
		public void run() {
			while (true) {
				Item item;
				try {
					item = queue.take();
				} catch (InterruptedException e) {
					System.err.println("closer interrupted while waiting for work");
					break;
				}
				if (item.rs != null) {
					try {
						item.rs.close();
					} catch (SQLException e) {
						System.err.println("closing result set failed: " + e.getMessage());
					}
				}
				if (item.conn != null) {
					try {
						item.conn.close();
					} catch (SQLException e) {
						System.err.println("closing connection failed: " + e.getMessage());
					}
				}
			}
		}

	}

	static class Item {
		Connection conn;
		ResultSet rs;

		Item(Connection conn, ResultSet rs) {
			this.conn = conn;
			this.rs = rs;
		}
	}
}
