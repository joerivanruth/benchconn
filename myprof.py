import time

class Run:
	__slots__ = 'start connected cursor executed retrieved closed'.split()

_MEASUREMENTS = None
_MEASUREMENTS_IDX = None
_CURRENT = Run()

def reserve(n):
	global _MEASUREMENTS
	global _MEASUREMENTS_IDX
	_MEASUREMENTS = [Run() for i in range(n)]
	_MEASUREMENTS_IDX = 0


def start():
	global _MEASUREMENTS
	global _MEASUREMENTS_IDX
	global _CURRENT
	if _MEASUREMENTS is not None:
		_CURRENT = _MEASUREMENTS[_MEASUREMENTS_IDX]
		_MEASUREMENTS_IDX += 1
	else:
		_CURRENT = Run() # dummy


def reached(stage) -> Run:
	global _CURRENT
	setattr(_CURRENT, stage, time.perf_counter_ns())


def dump(code, filename):
	global _MEASUREMENTS
	global _MEASUREMENTS_IDX
	f = open(filename, 'w')
	print("code,run,step,took,elapsed", file=f)
	for i, run in enumerate(_MEASUREMENTS[:_MEASUREMENTS_IDX]):
		start_ns = getattr(run, Run.__slots__[0])
		prev_ns = None
		for attrname in Run.__slots__:
			value_ns = getattr(run, attrname)
			elapsed_ns = value_ns - start_ns
			took_ns = value_ns - prev_ns if prev_ns is not None else 0
			prev_ns = value_ns
			print(f"{code},{i},{attrname},{took_ns},{elapsed_ns}", file=f)
	f.close()



