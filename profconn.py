#!/usr/bin/env python3

import argparse
import os
import sys
import time

import myprof


argparser = argparse.ArgumentParser()
argparser.add_argument('-H', '--host', default='localhost')
argparser.add_argument('-p', '--port', type=int, default=50000)
argparser.add_argument('-d', '--database', required=True)
argparser.add_argument('-n', '--iterations', type=int, default=1000)
argparser.add_argument('code', choices=['old', 'new'])

args = argparser.parse_args()

dir = os.path.join(os.getcwd(), 'pymonetdb-' + args.code)
assert os.path.isdir(dir)
sys.path.insert(1, dir)
import pymonetdb
pymonetdb_path = pymonetdb.__path__[0]
if os.path.dirname(pymonetdb_path) != dir:
	raise SystemError(f"Wanted pymonetdb to come from {dir}, got {pymonetdb_path}")

from pymonetdb import connect


def attempt(ntimes):
	for i in range(ntimes):
		myprof.start()
		myprof.reached('start')
		conn = connect(database=args.database, hostname=args.host, port=args.port)
		myprof.reached('connected')
		c = conn.cursor()
		myprof.reached('cursor')
		c.execute("select 42")
		myprof.reached('executed')
		data = c.fetchall()
		myprof.reached('retrieved')
		c.close()
		conn.close()
		myprof.reached('closed')
	return ntimes

# Warmup
attempt(100)
print('warm')

myprof.reserve(args.iterations)
attempt(args.iterations)
print('done')

myprof.dump(args.code, args.code + '.csv')
