#!/usr/bin/env python3

import argparse
import os
from queue import Queue
import sys
import threading
import time

# import pymonetdb
# from pymonetdb import connect

# cwd = os.getcwd()
# pymonetdb_path = pymonetdb.__path__[0]
# if cwd not in pymonetdb_path:
# 	raise SystemError(f"Require pymonetdb path {pymonetdb_path} to contain cwd {cwd}")


argparser = argparse.ArgumentParser()
argparser.add_argument('-H', '--host', default='localhost')
argparser.add_argument('-p', '--port', type=int, default=50000)
argparser.add_argument('-d', '--database', required=True)
argparser.add_argument('-m', '--min-connections', type=int, default=1000)
argparser.add_argument('-M', '--max-connections', type=int)
argparser.add_argument('-t', '--min-time', type=float, default=5.0)
argparser.add_argument('-T', '--max-time', type=float)
argparser.add_argument('-l', '--label', required=True)
argparser.add_argument('-c', '--code', type=str, required=True, choices=['old', 'new'])
argparser.add_argument('--sync-close', action='store_true')

args = argparser.parse_args()
if not args.max_time and not args.max_connections:
	argparser.error("Either -M/--max-connections or -T/--max-time is required")


dir = os.path.join(os.getcwd(), 'pymonetdb-' + args.code)
assert os.path.isdir(dir)
sys.path.insert(1, dir)
import pymonetdb
pymonetdb_path = pymonetdb.__path__[0]
if os.path.dirname(pymonetdb_path) != dir:
	raise SystemError(f"Wanted pymonetdb to come from {dir}, got {pymonetdb_path}")

from pymonetdb import connect

closer_queue = Queue(50)
def service_closer_queue():
	global closer_queue
	while True:
		conn, cursor = closer_queue.get()
		cursor.close()
		conn.close()
background_closer = threading.Thread(target=service_closer_queue, daemon=True)
background_closer.start()



def attempt(ntimes):
	global closer_queue
	for i in range(ntimes):
		conn = connect(database=args.database, hostname=args.host, port=args.port)
		c = conn.cursor()
		c.execute("select 42")
		data = c.fetchall()
		if args.sync_close:
			c.close()
			conn.close()
		else:
			closer_queue.put((conn, c))
	return ntimes


min_conns = args.min_connections
max_conns = args.max_connections
if max_conns:
	min_conns = min(min_conns, max_conns)

min_seconds = args.min_time
max_seconds = args.max_time
if max_seconds:
	min_seconds = min(min_seconds, max_seconds)


# Warmup
attempt(100)

t0 = time.time()
conns = 0
while 1:
	duration = time.time() - t0
	if max_seconds and duration >= max_seconds and conns >= min_conns:
		break
	if max_conns and conns >= max_conns and duration >= min_seconds:
		break

	# if (max_seconds and duration >= max_seconds) or (max_conns and conns >= max_conns):
	# 	# only stop 
	
	# if duration >= min_seconds and conns >= min_conns:
	# 	# consider stopping
	# 	if max_seconds and duration >= max_seconds:
	# 		break
	# 	if max_conns and conns >= max_conns:
	# 		break
	conns += attempt(100)

t1 = time.time()
duration = t1 - t0
rate = conns / duration
print(f"{args.label},{conns},{duration},{rate}")
