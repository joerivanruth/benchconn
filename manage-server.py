#!/usr/bin/env python3

# Usage: manage-server.py -p PORT CMD ARGS...
#
# Start CMD and listen on PORT.
# If someone connects to PORT, kill the command, sleep 2 seconds
# and

import argparse
import logging
import os
import shlex
import signal
import socket
import subprocess
import threading
import time

import psutil

argparser = argparse.ArgumentParser()
argparser.add_argument('-l', '--listen-port', type=int, required=True,
                       help='connect to this port to kill the command')
argparser.add_argument('-p', '--verify-port', type=int,
                       help='consider the command running if it listens on this port')
argparser.add_argument('--verify-timeout', type=float,
                       help='how long to wait for the command to start listening on verify_port')
argparser.add_argument('-s', '--sleep', type=float, default=2.0,
                       help='additional time to wait before considering command to be running')
argparser.add_argument('cmd', nargs=argparse.REMAINDER,
                       help='command to run')


def start_command(cmd: list, verify_port=None, verify_timeout=None, sleep_time=0.0):
    cmd_desc = " ".join(shlex.quote(a) for a in cmd)
    logging.info(f"Starting {cmd_desc}")
    process = subprocess.Popen(cmd)

    # wait for the process to start responding on the verify_port, or to exit
    if verify_port:
        poll_interval = 0.2
        if verify_timeout:
            deadline = time.time() + verify_timeout
        else:
            deadline = None
        logging.info(
            f"Waiting for process to start listening on port {verify_port}")
        while not deadline or time.time() <= deadline:
            check_process(process)
            try:
                s = socket.create_connection(
                    ('localhost', verify_port), poll_interval)
                # if we got a connection we can stop polling
                s.close()
                break
            except socket.error:
                # to be expected, probably just not ready yet
                pass
        logging.info('Process is still running')

    check_process(process)
    if sleep_time and sleep_time > 0:
        logging.info(f"Waiting {sleep_time:.1f}s to see if process still ok")
        try:
            process.wait(sleep_time)
        except subprocess.TimeoutExpired:
            # great, it's still running
            pass
    check_process(process)

    logging.info("Process started succesfully")
    return process


def check_process(process):
    process.poll()
    if process.returncode is not None:
        logging.warning(f'Process ended with code {process.returncode}')
        raise Exception(f'Process ended prematurely')


class Listener:
    def __init__(self, port):
        self.lock = threading.Lock()
        self.port = port
        self.conns = []
        self.sock = socket.socket()

        self.sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        self.sock.bind(('', port))
        self.sock.listen(5)
        self.thread = threading.Thread(target=self.run, daemon=True)
        self.thread.start()

    def wants_restart(self):
        with self.lock:
            return len(self.conns) > 0

    def flush(self):
        with self.lock:
            if self.conns:
                logging.info(
                    f"Telling {len(self.conns)} connections that server has restarted")
                for c in self.conns:
                    c.close()
                self.conns = []

    def run(self):
        logging.info(f"Listening on port {self.port}")
        try:
            while True:
                (s, a) = self.sock.accept()
                with self.lock:
                    logging.info("Received kill request from {a}")
                    self.conns.append(s)
        finally:
            logging.error("Listener thread killed!")


def kill_existing(port):
    for conn in psutil.net_connections():
        if conn.type == socket.SOCK_STREAM and conn.laddr[1] == port:
            if conn.pid:
                logging.info(
                    f"Killing existing process #{conn.pid} on port {port}")
                os.kill(conn.pid, signal.SIGKILL)
                time.sleep(1)
            else:
                logging.warning(
                    "Another process has port {port}, can't kill it")
            break


args = argparser.parse_args()
logging.basicConfig(level=logging.INFO)
listener = Listener(args.listen_port)
if args.verify_port:
    kill_existing(args.verify_port)

while True:
    proc = start_command(args.cmd,
                         verify_port=args.verify_port,
                         verify_timeout=args.verify_timeout,
                         sleep_time=args.sleep)

    # signal everybody who wants to know that a fresh command is running on
    listener.flush()

    # await kill requests while keeping an eye on the health of the process
    while True:
        check_process(proc)
        if listener.wants_restart():
            break
        time.sleep(0.2)

    # kill the process so we can start another one
    logging.info("Killing the process")
    proc.kill()
    time.sleep(1)
